<?php namespace app\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator2;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $email;
    public $username;
    public $message;
    public $phone;
    public $verifyCode;

    public function rules()
    {
        return [
            [['email', 'message'], 'required', 'message' => 'Поля email и сообщение обязательны для заполнения'],
            [['username', 'message'], 'string', 'max' => 255],
            ['email', 'email', 'message' => 'Пожалуйта, укажите именно email'],
            [['phone'], 'match', 'pattern' => '/^\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/',
                'message' => 'Введите телефон правильно'],
            [['verifyCode'], ReCaptchaValidator2::class, 'secret' => '6LcHifEZAAAAAN_p600XzQDlbSs2ST1eVp8Fk4tu'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Имя',
            'message' => 'Сообщение',
            'phone' => 'Телефон',
            'verifyCode' => 'ReCAPTCHA',
        ];
    }
    
    public function contact() : bool
    {
        if ($this->validate()) {
            $email = Rules::getValue(Rules::EMAIL_RULE);
            $message = 'Сообщение от ' . ($this->username ? $this->username . ' ' : '') . $this->email . ' ' .
                ($this->phone ? 'телефон: ' . $this->phone . ' ' : '') . PHP_EOL . PHP_EOL . $this->message;

            $dbMessage = new Feedback();
            $dbMessage->email = $this->email;
            $dbMessage->username = $this->username ?? '';
            $dbMessage->phone = $this->phone ?? '';
            $dbMessage->message = $this->message;
            $dbMessage->save();

            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setSubject('Обратная связь')
                ->setTextBody($message)
                ->send();
            return true;
        }
        return false;
    }
}
