<?php namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "authors".
 *
 * @property int $id
 * @property string $name
 *
 * @property AuthorsForBooks[] $authorsForBooks
 */
class Authors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[AuthorsForBooks]].
     */
    public function getAuthorsForBooks() : ActiveQuery
    {
        return $this->hasMany(AuthorsForBooks::class, ['author_id' => 'id']);
    }

    public static function setAuthors(int $bookId, string $authors, bool $update = false)
    {
        $authorsArray = explode(',', $authors);
        if ($update) {
            AuthorsForBooks::deleteAll(['book_id' => $bookId]);
        }
        foreach($authorsArray as $one) {
            $name = trim($one);
            if ($name !== "") {
                $authorFromDB = Authors::find()->where(['name' => $name])->one();
                if ($authorFromDB) {
                    $authorId = $authorFromDB->id;
                } else {
                    $newAuthor = new Authors();
                    $newAuthor->name = $name;
                    $newAuthor->save();
                    $authorId = $newAuthor->id;
                }
                $relation = new AuthorsForBooks();
                $relation->book_id = $bookId;
                $relation->author_id = $authorId;
                $relation->save();
            }
        }
    }
}
