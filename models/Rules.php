<?php namespace app\models;

use Yii;

/**
 * This is the model class for table "rules".
 *
 * @property int $id
 * @property string $name
 * @property string $value
 */
class Rules extends \yii\db\ActiveRecord
{
    const COUNT_RULE = 'count';
    const URL_RULE = 'url';
    const EMAIL_RULE = 'email';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rules';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'value'], 'required'],
            [['name', 'value'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
        ];
    }

    public static function getValue(string $name) : string
    {
        $rule = self::find()->where(['name' => $name])->one();
        return $rule ? $rule['value'] : '';
    }

    public static function setValue(string $name, string $value) : bool
    {
        $rule = self::find()->where(['name' => $name])->one();
        $rule->value = $value;
        return $rule->save();
    }
}
