<?php namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "statuses".
 *
 * @property int $id
 * @property string $name
 *
 * @property Books[] $books
 */
class Statuses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'statuses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Books]].
     */
    public function getBooks() : ActiveQuery
    {
        return $this->hasMany(Books::class, ['status_id' => 'id']);
    }

    public static function setStatus(Books $model, string $status) : Books
    {
        $statusFromDB = self::find()->where(['name' => trim($status)])->one();
        if (!$statusFromDB) {
            $newStatus = new Statuses;
            $newStatus->name = trim($status);
            $newStatus->save();
            $statusId = $newStatus->id;
        } else {
            $statusId = $statusFromDB->id;
        }
        $model->status_id = $statusId;
        return $model;
    }
}
