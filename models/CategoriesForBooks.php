<?php namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "categories_for_books".
 *
 * @property int $id
 * @property int $category_id
 * @property int $book_id
 *
 * @property Books $book
 * @property Categories $category
 */
class CategoriesForBooks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories_for_books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'book_id'], 'required'],
            [['category_id', 'book_id'], 'integer'],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Books::className(), 'targetAttribute' => ['book_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'book_id' => 'Book ID',
        ];
    }

    /**
     * Gets query for [[Book]].
     */
    public function getBook() : ActiveQuery
    {
        return $this->hasOne(Books::class, ['id' => 'book_id']);
    }

    /**
     * Gets query for [[Category]].
     */
    public function getCategory() : ActiveQuery
    {
        return $this->hasOne(Categories::class, ['id' => 'category_id']);
    }
}
