<?php namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $name
 *
 * @property CategoriesForBooks[] $categoriesForBooks
 */
class Categories extends ActiveRecord
{
    const DEFAULT_CATEGORY = 'Новинки';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function getCategoriesForBooks() : ActiveQuery
    {
        return $this->hasMany(CategoriesForBooks::class, ['category_id' => 'id']);
    }

    public static function setCategory(int $bookId, string $categories, bool $update = false)
    {
        $categoriesArray = explode(',', $categories);
        if ($update) {
            CategoriesForBooks::deleteAll(['book_id' => $bookId]);
        }

        foreach($categoriesArray as $one) {
            $name = trim($one);
            if ($name !== "") {
                $categoryFromDB = Categories::find()->where(['name' => $name])->one();
                if ($categoryFromDB) {
                    $categoryId = $categoryFromDB->id;
                } else {
                    $newCategory = new Categories();
                    $newCategory->name = $name;
                    $newCategory->save();
                    $categoryId = $newCategory->id;
                }
                $relation = new CategoriesForBooks();
                $relation->book_id = $bookId;
                $relation->category_id = $categoryId;
                $relation->save();
            }
        }
    }
}
