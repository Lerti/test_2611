<?php namespace app\models;

use yii\data\ActiveDataProvider;

class BooksSearch extends Books
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'picture', 'isbn'], 'string'],
            [['pages', 'status_id'], 'integer'],
            [['publish_date'], 'safe'],
            [['short_description'], 'string', 'max' => 3000],
            [['long_description'], 'string', 'max' => 6000],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Statuses::class, 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    public function search(array $params, int $pageSize = 20, bool $fromAdmin = false) : ActiveDataProvider
    {
        $query = Books::find()
            ->leftJoin(CategoriesForBooks::tableName(), Books::tableName() . '.id = ' . CategoriesForBooks::tableName() . '.book_id')
            ->with('authorForBook')
            ->with('status');

        if (!$fromAdmin) {
            $query->where(['category_id' => $params['category']]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (isset($params['author_name']) && $params['author_name']) {
            $query
                ->leftJoin(AuthorsForBooks::tableName(), Books::tableName() . '.id = ' . AuthorsForBooks::tableName() . '.book_id')
                ->leftJoin(Authors::tableName(), Authors::tableName() . '.id = ' . AuthorsForBooks::tableName() . '.author_id')
                ->andWhere([
                    'like', Authors::tableName() . '.name', $params['author_name']
                ]);
        }
        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['status_id' => $this->status_id]);

        return $dataProvider;
    }
}
