<?php namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property string $title
 * @property int $isbn
 * @property int $pages
 * @property string $publish_date
 * @property string $short_description
 * @property string $long_description
 * @property string|null $picture
 * @property int|null $status_id
 * @property UploadedFile $img
 *
 * @property AuthorsForBooks[] $authorsForBooks
 * @property Statuses $status
 * @property CategoriesForBooks[] $categoriesForBooks

 */
class Books extends \yii\db\ActiveRecord
{
    private $img;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'isbn'], 'string'],
            [['picture'], 'file', 'extensions' => 'jpg, png, jpeg'],
            [['pages', 'status_id'], 'integer'],
            [['publish_date'], 'safe'],
            [['short_description'], 'string', 'max' => 3000],
            [['long_description'], 'string', 'max' => 6000],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Statuses::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'isbn' => 'Артикул',
            'pages' => 'Количество страниц',
            'publish_date' => 'Дата публикации',
            'picture' => 'Изобажение',
            'status_id' => 'Status ID',
            'short_description' => 'Краткое описание',
            'long_description' => 'Полное описание',
        ];
    }

    /**
     * Gets query for [[AuthorsForBooks]].
     */
    public function getAuthorsForBooks() : ActiveQuery
    {
        return $this->hasMany(AuthorsForBooks::class, ['book_id' => 'id']);
    }

    /**
     * Gets query for [[Status]].
     */
    public function getStatus() : ActiveQuery
    {
        return $this->hasOne(Statuses::class, ['id' => 'status_id']);
    }

    /**
     * Gets query for [[CategoriesForBooks]]
     */
    public function getCategoriesForBooks() : ActiveQuery
    {
        return $this->hasMany(CategoriesForBooks::class, ['book_id' => 'id']);
    }


    public function getAuthorForBook() : ActiveQuery
    {
        return $this->hasMany(Authors::class, ['id' => 'author_id'])
            ->viaTable(AuthorsForBooks::tableName(), ['book_id' => 'id']);
    }

    public function getCategoryForBook() : ActiveQuery
    {
        return $this->hasMany(Categories::class, ['id' => 'category_id'])
            ->viaTable(CategoriesForBooks::tableName(), ['book_id' => 'id']);
    }

    public function updatePicture(string $picture) {
        $this->img = UploadedFile::getInstance($this, 'picture');
        if (!is_null($this->img)) {
            $path = '@app/web/images/' . $this->title . '=' . $picture;
            $this->img->saveAs($path);
            $this->picture = $this->title . '=' . $picture;
        }
    }
}
