<?php namespace app\commands;

use app\models\Authors;
use app\models\AuthorsForBooks;
use app\models\Books;
use app\models\Categories;
use app\models\CategoriesForBooks;
use app\models\Rules;
use app\models\Statuses;
use yii\console\Controller;
use yii\console\ExitCode;

class ParserController extends Controller
{
    public function actionGetBooks() : int
    {
        $url = Rules::getValue(RULES::URL_RULE);
        $site = file_get_contents($url);
        $books = json_decode($site, true);
        foreach ($books as $array) {
            $book = Books::find()->where(['title' => $array['title']])->one();
            if (!$book) {
                $category = key_exists('categories', $array) && $array['categories'] !== [] ? $array['categories'] : [Categories::DEFAULT_CATEGORY];
                $categoryFromDB = self::setBookProperty($category, 'category');
                $authorsFromDB = self::setBookProperty($array['authors'],'author');
                $statusFromDB = self::setBookProperty([$array['status']], 'status');
                $book = new Books();
                $book->title = $array['title'];
                $book->isbn = key_exists('isbn', $array) ? $array['isbn'] : '';
                $book->pages = key_exists('pageCount', $array) ? $array['pageCount'] : '';
                $book->publish_date = key_exists('publishedDate', $array) ? $array['publishedDate']['$date'] : '';
                $book->short_description = key_exists('shortDescription', $array) ? $array['shortDescription'] : '';
                $book->long_description = key_exists('longDescription', $array) ? $array['longDescription'] : '';

                if (key_exists('thumbnailUrl', $array)) {
                    $bookName = str_replace('/', '', strrchr($array['thumbnailUrl'], '/'));
                    $url = $array['thumbnailUrl'];
                    $path = './web/images/' . $bookName;
                    file_put_contents($path, file_get_contents($url));
                    $book->picture = $bookName;
                } else {
                    $book->picture = '';
                }
                $book->status_id = $statusFromDB[0];
                $book->save();
                $bookId = $book->id;
                self::setBookPropertyArray($categoryFromDB, false, 'category_id', $bookId);
                self::setBookPropertyArray($authorsFromDB, true, 'author_id', $bookId);
            }
        }
        print_r('книги успешно получены.' . PHP_EOL);
        return ExitCode::OK;
    }

    public static function setBookProperty(array $properties, string $table) : array
    {
        $id = [];
        foreach ($properties as $property) {
            $param = trim($property);
            $fromDB = ($table === 'author' ? Authors::find() :
                ($table === 'status' ? Statuses::find() : Categories::find()))->where(['name' => $param])->one();
            if (!$fromDB) {
                $new = ($table === 'author' ? new Authors() :
                    ($table === 'status' ? new Statuses() : new Categories()));
                $new->name = $param;
                $new->save();
                $id[] = $new->id;
            } else {
                $id[] = $fromDB->id;
            }
        }
        return $id;
    }

    public static function setBookPropertyArray(array $array, bool $authorTable, string $name, int $bookId)
    {
        foreach ($array as $value) {
            $new = ($authorTable ? new AuthorsForBooks() : new CategoriesForBooks());
            $new->$name = $value;
            $new->book_id = $bookId;
            $new->save();
        }
    }
}
