<?php

use yii\db\Migration;

/**
 * Class m201126_122943_add_tables
 */
class m201126_122943_add_tables extends Migration
{
    const CATEGORIES_TABLE = 'categories';
    const AUTHORS_TABLE = 'authors';
    const STATUS_TABLE = 'statuses';

    const BOOKS_TABLE = 'books';
    const BOOKS_CATEGORIES_TABLE = 'categories_for_books';
    const BOOKS_AUTHORS_TABLE = 'authors_for_books';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::CATEGORIES_TABLE, [
            'id' => $this->primaryKey(),
            'name' =>  $this->string()->notNull(),
        ]);
        $this->createTable(self::AUTHORS_TABLE, [
            'id' => $this->primaryKey(),
            'name' =>  $this->string()->notNull(),
        ]);
        $this->createTable(self::STATUS_TABLE, [
            'id' => $this->primaryKey(),
            'name' =>  $this->string()->notNull(),
        ]);

        $this->createTable(self::BOOKS_TABLE, [
            'id' => $this->primaryKey(),
            'title' =>  $this->string()->notNull(),
            'isbn' => $this->string(),
            'pages' => $this->integer(4),
            'publish_date' => $this->date(),
            'short_description' => $this->string(3000),
            'long_description' => $this->string(6000),
            'picture' => $this->string(),
            'status_id' => $this->integer(),
        ]);
        $this->addForeignKey(
            'fk_book_status',
            self::BOOKS_TABLE,
            ['status_id'],
            self::STATUS_TABLE,
            'id'
        );

        $this->createTable(self::BOOKS_AUTHORS_TABLE, [
            'id' => $this->primaryKey(),
            'author_id' =>  $this->integer()->notNull(),
            'book_id' =>  $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            'fk_authors_for_books_author',
            self::BOOKS_AUTHORS_TABLE,
            ['author_id'],
            self::AUTHORS_TABLE,
            'id'
        );
        $this->addForeignKey(
            'fk_authors_for_books_book',
            self::BOOKS_AUTHORS_TABLE,
            ['book_id'],
            self::BOOKS_TABLE,
            'id'
        );

        $this->createTable(self::BOOKS_CATEGORIES_TABLE, [
            'id' => $this->primaryKey(),
            'category_id' =>  $this->integer()->notNull(),
            'book_id' =>  $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            'categories_for_books_category',
            self::BOOKS_CATEGORIES_TABLE,
            ['category_id'],
            self::CATEGORIES_TABLE,
            'id'
        );
        $this->addForeignKey(
            'categories_for_books_book',
            self::BOOKS_CATEGORIES_TABLE,
            ['book_id'],
            self::BOOKS_TABLE,
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::BOOKS_CATEGORIES_TABLE);
        $this->dropTable(self::BOOKS_AUTHORS_TABLE);
        $this->dropTable(self::BOOKS_TABLE);
        $this->dropTable(self::STATUS_TABLE);
        $this->dropTable(self::AUTHORS_TABLE);
        $this->dropTable(self::CATEGORIES_TABLE);
    }
}
