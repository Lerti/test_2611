<?php

use yii\db\Migration;

/**
 * Class m201127_202938_add_rules_table
 */
class m201127_202938_add_rules_table extends Migration
{
    const RULES_TABLE = 'rules';
    const RULES_ARRAY = [
        ['count', 10],
        ['url', 'https://gitlab.com/prog-positron/test-app-vacancy/-/raw/master/books.json'],
        ['email', '*@mail.ru']
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::RULES_TABLE, [
            'id' => $this->primaryKey(),
            'name' =>  $this->string()->notNull(),
            'value' =>  $this->string()->notNull(),
        ]);

        $this->batchInsert(self::RULES_TABLE, ['name', 'value'], self::RULES_ARRAY);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::RULES_TABLE);
    }
}
