<?php

use yii\db\Migration;

/**
 * Class m201128_130131_add_users_table
 */
class m201128_130131_add_users_table extends Migration
{
    const USERS_TABLE = 'users_test';
    const USERS_INFO = [
        ['admin', 'admin', 'testAdminkey', 'admin-token'],
        ['guest', 'guest', 'testGuestkey', 'guest-token'],
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::USERS_TABLE, [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'authKey' => $this->string()->notNull(),
            'accessToken' => $this->string()->notNull(),
        ]);
        $this->batchInsert(self::USERS_TABLE, ['username', 'password', 'authKey', 'accessToken'],
            self::USERS_INFO);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::USERS_TABLE);
    }
}
