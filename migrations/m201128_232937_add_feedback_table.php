<?php

use yii\db\Migration;

/**
 * Class m201128_232937_add_feedback_table
 */
class m201128_232937_add_feedback_table extends Migration
{
    const FEEDBACK_TABLE = 'feedback';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::FEEDBACK_TABLE, [
            'id' => $this->primaryKey(),
            'email' =>  $this->string()->notNull(),
            'username' => $this->string(),
            'message' => $this->string()->notNull(),
            'phone' => $this->string(18),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::FEEDBACK_TABLE);
    }
}
