<?php namespace app\controllers;

use app\models\Authors;
use app\models\AuthorsForBooks;
use app\models\Books;
use app\models\BooksSearch;
use app\models\Categories;
use app\models\CategoriesForBooks;
use app\models\Feedback;
use app\models\Rules;
use app\models\Statuses;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

class AdminController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if ($action->id=='login')
                $this->layout = 'login';
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $rule
     * @return string|\yii\web\Response
     */
    public function actionRule(string $rule)
    {
        $model = Rules::find()->where(['name' => $rule])->one();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('changeRule' . $rule);
            return $this->refresh();
        }
        return $this->render('rule', [
            'model' => $model,
        ]);
    }

    public function actionFeedback() : string
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Feedback::find()
        ]);
        return $this->render('feedback', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBooks() : string
    {
        $searchModel = new BooksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 20, true);
        return $this->render('../site/books', [
            'fromAdmin' => true,
            'category' => '',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statuses' => ['' => ''] + ArrayHelper::map(Statuses::find()->orderBy('id')->all(), 'id', 'name'),
            'authorSearch' => isset(Yii::$app->request->queryParams['author_name']) ?
                Yii::$app->request->queryParams['author_name'] : ''
        ]);
    }

    public function actionModifyBook(int $id) : string
    {
        $book = Books::find()
            ->with('status')
            ->with('authorForBook')
            ->with('categoryForBook')
            ->where(['id' => $id ])->one();
        return $this->render('view', [
            'model' => $book,
        ]);
    }

    public function actionAddBook() : string
    {
        if (Yii::$app->request->isPost) {
            $model = new Books();
            $post = Yii::$app->request->post();
            $model->load($post);
            $modelFromDB = Books::find()->where(['title' => $model->title])->one();
            if ($modelFromDB) {
                return $this->render('view', [
                    'message' => true,
                    'model' => $modelFromDB,
                ]);
            } else {
                $model->updatePicture($_FILES['Books']['name']['picture']);
                if (isset($post['status']) && $post['status'] !== "") {
                    $model = Statuses::setStatus($model, $post['status']);
                }
                $model->save();
                $modelId = $model->id;
                $categories = isset($post['categories']) && $post['categories'] !== "" ? $post['categories'] : Categories::DEFAULT_CATEGORY;
                Categories::setCategory($modelId, $categories);

                if (isset($post['authors']) && $post['authors'] !== "") {
                    Authors::setAuthors($modelId, $post['authors']);
                }
                $book = Books::find()
                    ->with('status')
                    ->with('authorForBook')
                    ->with('categoryForBook')
                    ->where(['id' => $modelId])->one();
                return $this->render('view', [
                    'model' => $book,
                ]);
            }
        }

        return $this->render('create', [
            'model' => new Books(),
        ]);
    }

    public function actionDeleteBook(int $id) : Response
    {
        CategoriesForBooks::deleteAll(['book_id' => $id]);
        AuthorsForBooks::deleteAll(['book_id' => $id]);
        Books::find()->where(['id' => $id])->one()->delete();

        return $this->redirect(['/admin/books']);
    }

    public function actionUpdateBook(int $id) : string
    {
        $book = Books::find()
            ->with('status')
            ->with('authorForBook')
            ->with('categoryForBook')
            ->where(['id' => $id])->one();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            if (isset($post['Books']['picture']) && $post['Books']['picture'] === '') {
                unset($post['Books']['picture']);
            }
            $book->load($post);
            if ($_FILES['Books']['name']['picture'] !== '') {
                $book->updatePicture($_FILES['Books']['name']['picture']);
            }
            if (isset($post['status']) && $post['status'] !== "") {
                $book = Statuses::setStatus($book, $post['status']);
            }
            $book->save();
            $categories = isset($post['categories']) && $post['categories'] !== "" ? $post['categories'] : Categories::DEFAULT_CATEGORY;
            Categories::setCategory($id, $categories, true);
            if (isset($post['authors']) && $post['authors'] !== "") {
                Authors::setAuthors($id, $post['authors'], true);
            }
            return $this->render('view', [
                'model' => Books::find()->with('status')->with('authorForBook')->with('categoryForBook')->where(['id' => $id])->one(),
            ]);
        }

        return $this->render('update', [
            'model' => $book,
        ]);
    }
}
