<?php namespace app\controllers;

use app\models\Books;
use app\models\BooksSearch;
use app\models\Categories;
use app\models\CategoriesForBooks;
use app\models\Rules;
use app\models\Statuses;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if ($action->id=='login')
                $this->layout = 'login';
            return true;
        } else {
            return false;
        }
    }

    public function actionIndex() : string
    {
        $categories = Categories::find()->all();
        return $this->render('index', ['categories' => $categories]);
    }

    public function actionBooks(int $category) : string
    {
        $categoryFromDB = Categories::find()->where(['id' => $category])->one();
        $pageSize = Rules::getValue(Rules::COUNT_RULE);
        $searchModel = new BooksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $pageSize);
        return $this->render('books', [
            'fromAdmin' => false,
            'category' => $categoryFromDB,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statuses' => ['' => ''] + ArrayHelper::map(Statuses::find()->orderBy('id')->all(), 'id', 'name'),
            'authorSearch' => isset(Yii::$app->request->queryParams['author_name']) ?
                Yii::$app->request->queryParams['author_name'] : ''
        ]);
    }

    public function actionBook(int $id, int $category) : string
    {
        $book = Books::find()
            ->with('categoryForBook')
            ->where(['id' => $id ])->one();
        return $this->render('book', [
            'model' => $book,
            'category' => Categories::find()->where(['id' => $category])->one(),
            'otherBooks' => Books::find()
                ->leftJoin(CategoriesForBooks::tableName(), Books::tableName() . '.id = ' . CategoriesForBooks::tableName() . '.book_id')
                ->where(['category_id' => $category])
                ->andWhere(['!=', Books::tableName() . '.id', $id])
                ->all()
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout() : Response
    {
        Yii::$app->user->logout();
        return $this->redirect(Yii::$app->user->loginUrl);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        $email = Rules::getValue(Rules::EMAIL_RULE);
        if ($model->load(Yii::$app->request->post()) && $model->contact($email)) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
}
