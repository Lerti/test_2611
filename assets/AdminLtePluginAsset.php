<?php namespace app\assets;

use yii\web\AssetBundle;
class AdminLtePluginAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins';
    public $css = [
        'chart.js/Chart.min.css',
    ];
    public $js = [
        'chart.js/Chart.bundle.min.js'
    ];
    public $depends = [
        'dmstr\adminlte\web\AdminLteAsset',
    ];
}
