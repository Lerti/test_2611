<?php

/* @var $content string */

echo $this->render(
    'main-login',
    ['content' => $content]
);
