<?php

use app\models\Rules;
use dmstr\widgets\Menu;

?>

<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist') ?>/img/user2-160x160.jpg"
                     class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->isGuest ? 'Guest' : Yii::$app->user->identity->username; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => '', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Изменение книг',
                        'icon' => 'share',
                        'url' => ['admin/books']
                    ],
                    [
                        'label' => 'Настройки',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'На странице книг', 'icon' => 'file-code-o', 'url' => ['admin/rule', 'rule' => Rules::COUNT_RULE],],
                            ['label' => 'Url для парсинга книг', 'icon' => 'file-code-o', 'url' => ['admin/rule', 'rule' => Rules::URL_RULE],],
                            ['label' => 'Email получателя писем', 'icon' => 'file-code-o', 'url' => ['admin/rule', 'rule' => Rules::EMAIL_RULE],],
                            ['label' => 'Просмотр заявок', 'icon' => 'file-code-o', 'url' => ['admin/feedback'],],
                        ],
                    ],
                    ['label' => 'Обратная связь', 'icon' => 'file-code-o', 'url' => ['/contact'],],
                    Yii::$app->user->isGuest ? (
                            ['label' => 'Войти', 'url' => ['/site/login']]
                    ) : (
                            ['label' => 'Выйти', 'url' => ['/site/logout']]
                    )
                ],
            ]
        ) ?>
    </section>
</aside>
