<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model app\models\Books
 * @var $form yii\widgets\ActiveForm
 */

$update = (isset($update)) ? $update : false;
$author = [];
foreach ($model->authorForBook as $value) {
    $author[] = $value->name;
}
$author = implode(', ', $author);
$category = [];
foreach ($model->categoryForBook as $value) {
    $category[] = $value->name;
}
$category = implode(', ', $category);
?>

<div class="books-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isbn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pages')->input('number', ['max' => 9999, 'min' => 0]) ?>

    <?= $form->field($model, 'publish_date')->input('date') ?>

    <?= $model->picture ? '<p class = "control-label"><b>Текущая обложка: </b></p>
    <img src = "' . '/images/' . $model->picture . '" alt = "' . $model->title . '">' : ''; ?>
    <?= $form->field($model, 'picture')->fileInput(['class' => 'url-file-input'])->label('Выберете обложку: ') ?>

    <?= $form->field($model, 'short_description')->textArea(['maxlength' => true]) ?>

    <?= $form->field($model, 'long_description')->textArea(['maxlength' => true]) ?>

    <?= Html::label('Статус', 'status', ['class' => 'control-label']) ?>
    <?= Html::textInput('status', $update ? ($model->status->name ?? '') : '', ['class' => 'form-control']) ?>

    <?= Html::label('Категории', 'categories', ['class' => 'control-label']) ?>
    <?= Html::textInput('categories', $category, ['class' => 'form-control']) ?>

    <?= Html::label('Авторы', 'authors', ['class' => 'control-label']) ?>
    <?= Html::textInput('authors', $author, ['class' => 'form-control']) ?>
    <p class="control-label">При вводе <b>нескольких Категорий или Авторов</b> для книги указывайте значения <b>через ","</b> !</p>

    <div class="form-group">
        <?= Html::submitButton('Save', ['id' => 'user-save', 'class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
