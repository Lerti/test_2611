<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use app\models\Authors;

/**
 * @var $this yii\web\View
 * @var $model app\models\Books
 * @var $authors Authors
 */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Все книги', 'url' => ['admin/books']];
$this->params['breadcrumbs'][] = $this->title;

$message = isset($message) ? $message : false;
?>

<div class="books-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if($message) : ?>
        <div class="alert alert-success">
            Книга с таким названием уже существует!
        </div>
    <?php endif; ?>
    <p>
        <?= Html::a('Изменить', ['admin/update-book', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['admin/delete-book', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную книгу?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'isbn',
            'pages',
            'publish_date',
            'short_description',
            'long_description',
            [
                'label' => 'Обложка',
                'value' => function ($model) {
                    return $model->picture ? '<img src = "' . '/images/' . $model->picture . '" alt = "' . $model->title . '">' : '';
                },
                'format' => 'raw'
            ],
            [
                'label' => 'Статус',
                'value' => function ($data) {
                    return $data->status->name ?? '';
                },
                'format' => 'raw'
            ],
            [
                'label' => 'Авторы',
                'value' => function ($data) {
                    $author = [];
                    foreach ($data->authorForBook as $value) {
                        $author[] = $value->name;
                    }
                    return implode(', ', $author);
                },
                'format' => 'raw'
            ],
            [
                'label' => 'Категории',
                'value' => function ($model) {
                    $answer = '';
                    foreach ($model->categoryForBook as $category) {
                        $answer .= '<a href = "' . Url::to(['site/books', 'category' => $category->id]) . '">' . $category->name . '</a><br>';
                    }
                    return $answer;
                },
                'format' => 'raw'
            ],
        ],
    ]) ?>
</div>
