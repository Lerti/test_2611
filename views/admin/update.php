<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model app\models\Books
 */

$this->title = 'Обновить книгу ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Все книги', 'url' => ['/admin/books']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'update' => true,
        'model' => $model,
    ]) ?>
</div>
