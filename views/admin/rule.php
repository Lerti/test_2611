<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Rules;

/**
 * @var $this yii\web\View
 * @var $form ActiveForm
 * @var $model Rules
 */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1>Изменение параметра <?= $model->name ?></h1>

    <?php if (Yii::$app->session->hasFlash('changeRule' . $model->name)): ?>
        <div class="alert alert-success">
            Изменение данного параметра прошло успешно.
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'rule-form']); ?>
            <?= $model->name === Rules::URL_RULE ?
                $form->field($model, 'value')->input('url') :
                ($model->name === Rules::EMAIL_RULE ? $form->field($model, 'value')->input('email') :
                    $form->field($model, 'value')->input('number'))
            ?>
            <div class="form-group">
                <?= Html::submitButton('Изменить', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
