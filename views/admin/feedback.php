<?php

use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/**
 * @var $this yii\web\View
 * @var $dataProvider ActiveDataProvider
 */

$this->title = 'Просмотр заявок с формы обратной связи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <h1><?= $this->title ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
    ]); ?>
</div>
