<?php

use app\models\Categories;
use app\models\Books;
use yii\helpers\Url;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model Books
 * @var $category Categories
 * @var $otherBooks Books
 */

$this->title = 'Книга ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => Url::to(['site/books', 'category' => $category->id])];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-index">
    <h1><?= $this->title ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'isbn',
            [
                'label' => 'Обложка',
                'value' => function ($model) {
                    return $model->picture ? '<img src = "' . '/images/' . $model->picture . '" alt = "' . $model->title . '">' : '';
                },
                'format' => 'raw'
            ],
            [
                'label' => 'Категории',
                'value' => function ($model) {
                    $answer = '';
                    foreach ($model->categoryForBook as $category) {
                        $answer .= '<a href = "' . Url::to(['site/books', 'category' => $category->id]) . '">' . $category->name . '</a><br>';
                    }
                    return $answer;
                },
                'format' => 'raw'
            ],
            [
                'label' => 'Книги из текущей категории',
                'value' => function () use ($otherBooks, $category) {
                    $answer = '';
                    foreach ($otherBooks as $book) {
                        $answer .= '<a href = "' . Url::to(['site/book', 'id' => $book->id, 'category' => $category->id]) . '">' . $book->title . '</a><br>';
                    }
                    return $answer;
                },
                'format' => 'raw'
            ],
        ],
    ]); ?>
</div>
