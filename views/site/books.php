<?php

use app\models\Categories;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\Html;
use app\models\BooksSearch;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View
 * @var $category Categories
 * @var $searchModel BooksSearch
 * @var $dataProvider ActiveDataProvider
 * @var $statuses array
 * @var $authorSearch string
 * @var $fromAdmin bool
 */

$this->title = $fromAdmin ? 'Все книги' : 'Книги из категории ' . $category->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <h1><?= $this->title ?></h1>

    <div>
        <?php if($fromAdmin) : ?>
        <a href="<?= Url::to(['/admin/add-book'])?>" class="btn btn-success">Добавить книгу</a>
        <?php endif; ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'format' => 'raw',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status_id',
                    $statuses,
                    ['class' => 'form-control']
                ),
                'value' => function ($data) {
                    return $data->status->name ?? '';
                },
            ],
            [
                'attribute' => 'author_name',
                'label' => 'Автор',
                'filter' => Html::input('string', 'author_name', $authorSearch, ['class' => 'form-control']),
                'value' => function ($data) {
                    $author = [];
                    foreach ($data->authorForBook as $value) {
                        $author[] = $value->name;
                    }
                    return implode(', ', $author);
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Перейти',
                'value' => function ($data) use ($category, $fromAdmin) {
                    return '<a href = "' . ($fromAdmin ? Url::to(['admin/modify-book', 'id' => $data->id]) :
                        Url::to(['site/book', 'id' => $data->id, 'category' => $category->id]))
                        . '">==></a>';
                },
                'format' => 'raw',
            ],
        ],
    ]); ?>
</div>
