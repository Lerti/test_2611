<?php

use app\models\Categories;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $categories Categories[]
 */

$this->title = 'Главная страница';
?>
<div class="site-index">
    <h1>Категории книг</h1>
    <div class="body-content">
        <?php foreach ($categories as $category) : ?>
        <div><a href="<?= Url::to(['site/books', 'category' => $category->id]) ?>"><b><?= $category->name ?></b></a></div>
        <?php endforeach; ?>
    </div>
</div>
